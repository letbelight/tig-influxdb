FROM influxdb
ADD data.txt /etc/influxdb
RUN /etc/init.d/influxdb restart &&  influx -import -path=/etc/influxdb/data.txt && influx -execute 'show users' && /etc/init.d/influxdb  stop
USER influxdb 
# volume - /var/lib/influxdb
